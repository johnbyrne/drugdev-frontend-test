import React from 'react';
import renderer from 'react-test-renderer';
import ContactList from './ContactList';
import { MockedProvider } from 'react-apollo/test-utils';
import { BrowserRouter } from 'react-router-dom';

describe('Contact List', () => {
    it ('Renders the component', () => {
        let component = renderer.create(
            <BrowserRouter>
                <MockedProvider addTypename={false}>
                    <ContactList />
                </MockedProvider>
            </BrowserRouter>
        ).toJSON();
        expect(component).toMatchSnapshot();
    });
});
