import React, { Component, useState } from "react";
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

interface ContactParamsNumber {
  id: string;
}

interface ContactParams {
  params: ContactParamsNumber;
}

interface ContactEditProps {
  match: ContactParams;
}

const UPDATE_CONTACT = gql`
  mutation UpdateContact($id: ID!, $name: String!, $email: String!) {
    updateContact(contact: { id: $id, name: $name, email: $email }) {
      id
      name
      email
    }
  }
`;

const ContactEdit = (props: ContactEditProps) => {
  const contactId = props.match.params.id;
  const classes = useStyles();
  return (
    <Query
      query={gql`
          {
            contact(id:${contactId}) {
              id
              name
              email
            }
          }
        `}
    >
      {({ loading, error, data }) => {
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error</p>;
        const { id, name, email } = data.contact;
        const contact = { id, name, email };
        let inputName: HTMLInputElement | null;
        let inputEmail: HTMLInputElement | null;
        return (
          <div className={classes.container}>
            <p>
              Name:
              <input className={classes.input}
                ref={node => {
                  inputName = node;
                }}
                placeholder={name}
              />
            </p>
            <p>
              Email:
              <input className={classes.input}
                ref={node => {
                  inputEmail = node;
                }}
                placeholder={email}
              />
            </p>
            <Mutation
              mutation={UPDATE_CONTACT}
            >
              {updateContact => (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={e => {
                    let nameValue = "";
                    if (inputName !== null) nameValue = inputName.value;
                    let emailValue = "";
                    if (inputEmail !== null) emailValue = inputEmail.value;
                    updateContact({
                      variables: { id: id, name: nameValue, email: emailValue }
                    });
                    window.location.href = '/';
                  }}
                >
                  Update Contact
                </Button>
              )}
            </Mutation>
          </div>
        );
      }}
    </Query>
  );
};

export default ContactEdit;

const useStyles = makeStyles({
  container: {
    marginLeft: 100,
    marginRight: 100
  },
  input: {
    margin: 10
  }
});
