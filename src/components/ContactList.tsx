import React, { Component } from "react";
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import { createMemoryHistory } from "history";

interface Contact {
  name: string;
  email: string;
  id: string;
}

const alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
const history = createMemoryHistory()

export default function ContactList({}: any, context: any) {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h1>Contacts</h1>
      <Link to={`/create-contact`}>
        <Button variant="contained" color="primary">
          New Contact
        </Button>
      </Link>
      <Query
        query={gql`
          {
            contacts {
              name
              email
              id
            }
          }
        `}
      >
        {({ loading, error, data }) => {
          if (loading) return <p>Loading...</p>;
          if (error) return <p>Error</p>;
          return alphabet.map(letter => {
            const contactsBySurname = data.contacts.filter(
              (contact: Contact) => {
                return (
                  contact.name
                    .split(" ")[1]
                    .charAt(0)
                    .toLowerCase() === letter
                );
              }
            );
            if (contactsBySurname.length > 0) {
              return (
                <div key={letter} className={classes.contactsByLetterContainer}>
                  <h1>{letter.toUpperCase()}</h1>
                  <Grid
                    className={classes.contactsByLetterGrid}
                    container
                    spacing={8}
                  >
                    {contactsBySurname.map(({ name, email, id }: Contact) => (
                      <Grid item xs={4} key={id}>
                        <Paper className={classes.paper}>
                          <Link to={`/contact/${id}`}>
                            <IconButton
                              className={classes.icon}
                              aria-label="Edit contact"
                            >
                              <EditIcon />
                            </IconButton>
                          </Link>
                          <h3>{name}</h3>
                          <h4>{email}</h4>
                          <Mutation
                            mutation={gql`
                              mutation {
                                deleteContact(id:${id})
                              }
                            `}
                          >
                            {deleteContact => (
                              <IconButton
                                className={classes.icon}
                                aria-label="Delete contact"
                                onClick={e => {
                                  deleteContact({
                                    variables: {
                                      id
                                    }
                                  });
                                  window.location.reload();
                                }}
                              >
                                <DeleteIcon />
                              </IconButton>
                            )}
                          </Mutation>
                        </Paper>
                      </Grid>
                    ))}
                  </Grid>
                </div>
              );
            } else {
              return "";
            }
          });
        }}
      </Query>
    </div>
  );
}

const useStyles = makeStyles({
  container: {
    marginLeft: 100,
    marginRight: 100
  },
  root: {
    flexGrow: 1
  },
  contactsByLetterContainer: {
    borderTop: "1px solid #ddd",
    marginTop: 10
  },
  contactsByLetterGrid: {
    justifyContent: "center",
    marginBottom: 10
  },
  icon: {
    padding: 2,
    display: "flex"
  },
  paper: {
    padding: 5,
    textAlign: "center",
    color: "#333",
    backgroundColor: "#64B5F6"
  },
  button: {
    marginTop: 20
  }
});
