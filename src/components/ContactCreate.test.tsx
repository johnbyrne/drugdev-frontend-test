import React from "react";
import renderer from "react-test-renderer";
import ContactCreate from "./ContactCreate";
import "whatwg-fetch";
import { BrowserRouter } from "react-router-dom";
import { MockedProvider } from "react-apollo/test-utils";

describe("Contact Create", () => {
    it ("Renders the component", () => {
        let component = renderer.create(
            <BrowserRouter>
                <MockedProvider addTypename={false}>
                    <ContactCreate />
                </MockedProvider>
            </BrowserRouter>
        ).toJSON();
        expect(component).toMatchSnapshot();
    });
});
