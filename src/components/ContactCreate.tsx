import React, { Component, useState } from "react";
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const ADD_CONTACT = gql`
  mutation AddContact($name: String!, $email: String!) {
    addContact(contact: {name: $name, email: $email}) {
      name
      email
    }
  }
`;

const ContactCreate = () => {
  const classes = useStyles();

  let inputName: HTMLInputElement | null;
  let inputEmail: HTMLInputElement | null;

  return (
    <div className={classes.container}>
      <p>
        Name:
        <input
          className={classes.input}
          ref={node => {
            inputName = node;
          }}
          placeholder="Enter name"
        />
      </p>
      <p>
        Email:
        <input
          className={classes.input}
          ref={node => {
            inputEmail = node;
          }}
          placeholder="Enter email"
        />
      </p>
      <Mutation mutation={ADD_CONTACT}>
        {addContact => (
          <Button
            variant="contained"
            color="primary"
            onClick={e => {
              let nameValue = "";
              if (inputName !== null) nameValue = inputName.value;
              let emailValue = "";
              if (inputEmail !== null) emailValue = inputEmail.value;
              addContact({
                variables: { name: nameValue, email: emailValue }
              });
              window.location.href = "/";
            }}
          >
            Create New Contact
          </Button>
        )}
      </Mutation>
    </div>
  );
};

export default ContactCreate;

const useStyles = makeStyles({
  container: {
    marginLeft: 100,
    marginRight: 100
  },
  input: {
    margin: 10
  }
});
