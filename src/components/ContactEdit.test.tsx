import React from "react";
import renderer from "react-test-renderer";
import ContactEdit from "./ContactEdit";
import { BrowserRouter } from "react-router-dom";
import { MockedProvider } from "react-apollo/test-utils";

describe("Contact Edit", () => {
    it ("Renders the component", () => {
        const match = {
            params: {
                id: ""
            }
        }
        let component = renderer.create(
            <BrowserRouter>
                <MockedProvider addTypename={false}>
                    <ContactEdit match={match} />
                </MockedProvider>
            </BrowserRouter>
        ).toJSON();
        expect(component).toMatchSnapshot();
    });
});
