import React, { Component } from "react";
import { ApolloProvider } from "react-apollo";
import ContactList from "./components/ContactList";
import ContactEdit from "./components/ContactEdit";
import ContactCreate from "./components/ContactCreate";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "cross-fetch/polyfill";
import ApolloClient from "apollo-boost";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#e5e5e5",
      main: "#727272",
      dark: "#363839",
      contrastText: "#fff"
    },
    secondary: {
      light: "#ff5e50",
      main: "#e41e26",
      dark: "#a90000",
      contrastText: "#fff"
    }
  }
});

const client = new ApolloClient({
  uri: "http://localhost:3001"
});

class App extends Component {
  componentDidMount() {}

  render() {
    return (
      <ThemeProvider theme={theme}>
        <ApolloProvider client={client}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={ContactList} />
              <Route path="/contact/:id" component={ContactEdit} />
              <Route path="/create-contact/" component={ContactCreate} />
            </Switch>
          </BrowserRouter>
        </ApolloProvider>
      </ThemeProvider>
    );
  }
}

export default App;
