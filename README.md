# Contact Manager

Tech stack consists of React Apollo for GraphQL.

To install, remove node_modules folder and package-lock.json then install with `yarn`.

Tests are written with `react-test-renderer`.

Currently `x-state` is not used.


### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run gql`

Runs the graphql server.

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
